# Changelog
## 1.2.0
- add SHOUTING_SNAKE_CASE and SHOUTING-KEBAB-CASE text support

## 1.1.1
- fix numbers in text error

## 1.1.0
- php8 compatibility

## 1.0.0
- initial version to convert camelCase, PascalCase, snake_case and kebab-case texts
