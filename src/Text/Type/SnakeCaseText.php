<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


class SnakeCaseText extends AbstractText
{
    use SnakeCaseStyleConvertTrait;

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return self::encode($this->getOriginalText());
    }

    /**
     * @inheritDoc
     */
    public static function createFromEncodedText(string $encodedText): TextInterface
    {
        return new static(self::decode($encodedText));
    }
}
