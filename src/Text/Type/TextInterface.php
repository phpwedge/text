<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


interface TextInterface
{
    /**
     * Returns the encoded format of the original text.
     *
     * @return string
     */
    public function getText(): string;

    /**
     * Returns the original text.
     *
     * @return string
     */
    public function getOriginalText(): string;

    /**
     * Returns the text instance.
     *
     * @param string $encodedText
     *
     * @return $this
     */
    public static function createFromEncodedText(string $encodedText): self;
}
