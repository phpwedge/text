<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


class CamelCaseText extends PascalCaseText
{
    use PascalCaseStyleConvertTrait;

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return lcfirst(self::encode($this->getOriginalText()));
    }

    /**
     * @inheritDoc
     */
    public static function createFromEncodedText(string $encodedText): TextInterface
    {
        return new static(self::decode($encodedText));
    }
}
