<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


trait PascalCaseStyleConvertTrait
{
    /**
     * Returns the PascalCase style encoded text.
     *
     * @param string $originalText
     *
     * @return string
     */
    private static function encode(string $originalText): string
    {
        $words = [];
        $word = '';
        $maxLength = strlen($originalText);
        for ($index = 0; $index < $maxLength; $index++) {
            $currentCharacter = $originalText[$index];
            if (in_array($currentCharacter, ['-', '_', ' '], true)) {
                $words[] = ucfirst($word);
                $word = '';
            } else {
                $word .= $currentCharacter;
            }
        }

        if (empty($word) === false) {
            $words[] = ucfirst($word);
        }

        return implode('', $words);
    }

    /**
     * Returns the decoded version of the text.
     *
     * @param string $encodedText
     *
     * @return string
     */
    private static function decode(string $encodedText): string
    {
        $originalText = '';
        $maxLength = strlen($encodedText);
        for ($index = 0; $index < $maxLength; $index++) {
            if (preg_match('#[A-Z]#', $encodedText[$index]) === 1) {
                $originalText .= ' ';
            }

            $originalText .= strtolower($encodedText[$index]);
        }

        return ltrim($originalText, ' ');
    }
}
