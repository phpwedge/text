<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


use Cocur\Slugify\Slugify;

class SlugifiedText extends AbstractText
{
    use SnakeCaseStyleConvertTrait;

    /** @var Slugify */
    private $slugify;

    /**
     * SlugifiedText constructor.
     *
     * @param string $originalText
     */
    public function __construct(string $originalText)
    {
        $this->slugify = new Slugify();

        parent::__construct($originalText);
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return $this->slugify->slugify($this->getOriginalText());
    }

    /**
     * @inheritDoc
     */
    public static function createFromEncodedText(string $encodedText): TextInterface
    {
        return new static(self::decode($encodedText, '-'));
    }
}
