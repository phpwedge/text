<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


trait ShoutingSnakeCaseStyleConvertTrait
{
    use SnakeCaseStyleConvertTrait;

    /**
     * Returns the SNAKE_CASE style encoded text.
     *
     * @param string $originalText
     * @param string $delimiter
     *
     * @return string
     */
    private static function encode(string $originalText, string $delimiter = '_'): string
    {
        $encodedText = '';
        $maxLength = strlen($originalText);
        for ($index = 0; $index < $maxLength; $index++) {
            $currentCharacter = $originalText[$index];
            if (in_array($currentCharacter, ['-', '_', ' '], true)) {
                $encodedText .= $delimiter;
            } else {
                $encodedText .= strtoupper($currentCharacter);
            }
        }

        return trim($encodedText, $delimiter);
    }
}
