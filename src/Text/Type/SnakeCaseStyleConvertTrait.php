<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


trait SnakeCaseStyleConvertTrait
{
    /**
     * Returns the snake_case style encoded text.
     *
     * @param string $originalText
     * @param string $delimiter
     *
     * @return string
     */
    private static function encode(string $originalText, string $delimiter = '_'): string
    {
        $encodedText = '';
        $maxLength = strlen($originalText);
        for ($index = 0; $index < $maxLength; $index++) {
            $currentCharacter = $originalText[$index];
            if (in_array($currentCharacter, ['-', '_', ' '], true)) {
                $encodedText .= $delimiter;
            } else {
                $encodedText .= strtolower($currentCharacter);
            }
        }

        return trim($encodedText, $delimiter);
    }

    /**
     * Returns the decoded version of the text.
     *
     * @param string $encodedText
     * @param string $delimiter
     *
     * @return string
     */
    private static function decode(string $encodedText, string $delimiter = '_'): string
    {
        $originalText = '';
        $maxLength = strlen($encodedText);
        for ($index = 0; $index < $maxLength; $index++) {
            if ($encodedText[$index] === $delimiter) {
                $originalText .= ' ';
            } else {
                $originalText .= strtolower($encodedText[$index]);
            }
        }

        return $originalText;
    }
}
