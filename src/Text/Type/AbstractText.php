<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


abstract class AbstractText implements TextInterface
{
    /**
     * @var string
     */
    private $originalText;

    /**
     * AbstractText constructor.
     *
     * @param string $originalText
     */
    public function __construct(string $originalText)
    {
        $this->originalText = $originalText;
    }

    /**
     * @inheritDoc
     */
    public function getOriginalText(): string
    {
        return $this->originalText;
    }
}
