<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Type;


class ShoutingKebabCaseText extends AbstractText
{
    use ShoutingSnakeCaseStyleConvertTrait;

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return self::encode($this->getOriginalText(), '-');
    }

    /**
     * @inheritDoc
     */
    public static function createFromEncodedText(string $encodedText): TextInterface
    {
        return new static(self::decode($encodedText, '-'));
    }
}
