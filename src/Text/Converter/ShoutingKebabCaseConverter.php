<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Converter;


use PhpWedge\Core\Text\Type\ShoutingKebabCaseText;
use PhpWedge\Core\Text\Type\TextInterface;

class ShoutingKebabCaseConverter implements ConverterInterface
{
    /**
     * @inheritDoc
     */
    public function isApplicable(string $text): bool
    {
        return (preg_match('#^([A-Z][A-Z0-9]*(-[A-Z][A-Z0-9]*)*)+$#', $text) === 1);
    }

    /**
     * @inheritDoc
     */
    public function getTextInstance(string $encodedText): TextInterface
    {
        return ShoutingKebabCaseText::createFromEncodedText($encodedText);
    }
}
