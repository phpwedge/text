<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Converter;


use PhpWedge\Core\Text\Type\CamelCaseText;
use PhpWedge\Core\Text\Type\TextInterface;

class CamelCaseConverter implements ConverterInterface
{
    /**
     * @inheritDoc
     */
    public function isApplicable(string $text): bool
    {
        return preg_match(
                '#^[a-z]([a-z0-9]+)?([A-Z]([a-z0-9]+)?){1,500}$#',
                $text
        ) === 1;
    }

    /**
     * @inheritDoc
     */
    public function getTextInstance(string $encodedText): TextInterface
    {
        return CamelCaseText::createFromEncodedText($encodedText);
    }
}
