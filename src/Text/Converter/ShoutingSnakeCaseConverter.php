<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Converter;


use PhpWedge\Core\Text\Type\ShoutingSnakeCaseText;
use PhpWedge\Core\Text\Type\SnakeCaseText;
use PhpWedge\Core\Text\Type\TextInterface;

class ShoutingSnakeCaseConverter implements ConverterInterface
{
    /**
     * @inheritDoc
     */
    public function isApplicable(string $text): bool
    {
        return (preg_match('#^([A-Z][A-Z0-9]*(_[A-Z][A-Z0-9]*)*)+$#', $text) === 1);
    }

    /**
     * @inheritDoc
     */
    public function getTextInstance(string $encodedText): TextInterface
    {
        return ShoutingSnakeCaseText::createFromEncodedText($encodedText);
    }
}
