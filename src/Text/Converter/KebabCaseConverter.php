<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Converter;


use PhpWedge\Core\Text\Type\KebabCaseText;
use PhpWedge\Core\Text\Type\TextInterface;

class KebabCaseConverter implements ConverterInterface
{
    /**
     * @inheritDoc
     */
    public function isApplicable(string $text): bool
    {
        return preg_match(
            '#^([a-z][a-z0-9]*(-[a-z][a-z0-9]*)*)+$#',
            $text
        ) === 1;
    }

    /**
     * @inheritDoc
     */
    public function getTextInstance(string $encodedText): TextInterface
    {
        return KebabCaseText::createFromEncodedText($encodedText);
    }
}
