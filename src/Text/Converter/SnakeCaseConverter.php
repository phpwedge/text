<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Converter;


use PhpWedge\Core\Text\Type\SnakeCaseText;
use PhpWedge\Core\Text\Type\TextInterface;

class SnakeCaseConverter implements ConverterInterface
{
    /**
     * @inheritDoc
     */
    public function isApplicable(string $text): bool
    {
        return preg_match(
            '#^([a-z][a-z0-9]*(_[a-z][a-z0-9]*)*)+$#',
            $text
        ) === 1;
    }

    /**
     * @inheritDoc
     */
    public function getTextInstance(string $encodedText): TextInterface
    {
        return SnakeCaseText::createFromEncodedText($encodedText);
    }
}
