<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text\Converter;


use PhpWedge\Core\Text\Type\TextInterface;

interface ConverterInterface
{
    /**
     * Returns TRUE if the given converter is applicable to use.
     *
     * @param string $text
     *
     * @return bool
     */
    public function isApplicable(string $text): bool;

    /**
     * Returns the text type instance.
     *
     * @param string $encodedText
     *
     * @return TextInterface
     */
    public function getTextInstance(string $encodedText): TextInterface;
}
