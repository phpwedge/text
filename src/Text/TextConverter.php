<?php

declare(strict_types=1);


namespace PhpWedge\Core\Text;


use PhpWedge\Core\Text\Converter\CamelCaseConverter;
use PhpWedge\Core\Text\Converter\ConverterInterface;
use PhpWedge\Core\Text\Converter\KebabCaseConverter;
use PhpWedge\Core\Text\Converter\PascalCaseConverter;
use PhpWedge\Core\Text\Converter\ShoutingKebabCaseConverter;
use PhpWedge\Core\Text\Converter\ShoutingSnakeCaseConverter;
use PhpWedge\Core\Text\Converter\SnakeCaseConverter;
use PhpWedge\Core\Text\Type\CamelCaseText;
use PhpWedge\Core\Text\Type\KebabCaseText;
use PhpWedge\Core\Text\Type\PascalCaseText;
use PhpWedge\Core\Text\Type\ShoutingKebabCaseText;
use PhpWedge\Core\Text\Type\ShoutingSnakeCaseText;
use PhpWedge\Core\Text\Type\SnakeCaseText;

class TextConverter
{
    /** @var $this */
    protected static $instance;

    /** @var ConverterInterface[] */
    private $converters;

    /**
     * TextConverter constructor.
     *
     * @param ConverterInterface[] $converters
     */
    public function __construct(array $converters)
    {
        $this->converters = $converters;
    }

    /**
     * Returns an instance of the text converter.
     *
     * @return $this
     */
    public static function getInstance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static(
                [
                    new CamelCaseConverter(),
                    new PascalCaseConverter(),
                    new SnakeCaseConverter(),
                    new KebabCaseConverter(),
                    new ShoutingSnakeCaseConverter(),
                    new ShoutingKebabCaseConverter(),
                ]
            );
        }

        return static::$instance;
    }

    /**
     * Converts the given text to the requested format.
     *
     * @param string $text
     * @param string $outputFormatFQN
     *
     * @return string
     */
    public function convert(string $text, string $outputFormatFQN): string
    {
        foreach ($this->converters as $converter) {
            if ($converter->isApplicable($text)) {
                return (new $outputFormatFQN(
                    $converter->getTextInstance($text)->getOriginalText()
                ))->getText();
            }
        }

        return (new $outputFormatFQN($text))->getText();
    }

    /**
     * Converts the given text to PascalCase.
     *
     * @param string $text
     *
     * @return string
     */
    public function convertToPascalCase(string $text): string
    {
        return $this->convert(
            $text,
            PascalCaseText::class
        );
    }

    /**
     * Converts the given text to camelCase.
     *
     * @param string $text
     *
     * @return string
     */
    public function convertToCamelCase(string $text): string
    {
        return $this->convert(
            $text,
            CamelCaseText::class
        );
    }

    /**
     * Converts the given text to snake_case.
     *
     * @param string $text
     *
     * @return string
     */
    public function convertToSnakeCase(string $text): string
    {
        return $this->convert(
            $text,
            SnakeCaseText::class
        );
    }

    /**
     * Converts the given text to kebab-case.
     *
     * @param string $text
     *
     * @return string
     */
    public function convertToKebabCase(string $text): string
    {
        return $this->convert(
            $text,
            KebabCaseText::class
        );
    }

    /**
     * Converts the given text to SHOUTING_SNAKE_CASE.
     *
     * @param string $text
     *
     * @return string
     */
    public function convertToShoutingSnakeCase(string $text): string
    {
        return $this->convert(
            $text,
            ShoutingSnakeCaseText::class
        );
    }

    /**
     * Converts the given text to SHOUTING-KEBAB-CASE.
     *
     * @param string $text
     *
     * @return string
     */
    public function convertToShoutingKebabCase(string $text): string
    {
        return $this->convert(
            $text,
            ShoutingKebabCaseText::class
        );
    }
}
