[![Codacy Badge](https://app.codacy.com/project/badge/Grade/eda6149f909b45f8be7cc0f89b3851bc)](https://www.codacy.com/bb/phpwedge/text/dashboard?utm_source=kchilly@bitbucket.org&amp;utm_medium=referral&amp;utm_content=phpwedge/text&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/eda6149f909b45f8be7cc0f89b3851bc)](https://www.codacy.com/bb/phpwedge/text/dashboard?utm_source=kchilly@bitbucket.org&utm_medium=referral&utm_content=phpwedge/text&utm_campaign=Badge_Coverage)
[![Latest Stable Version](https://poser.pugx.org/phpwedge/text/v/stable)](https://packagist.org/packages/phpwedge/text)
[![Total Downloads](https://poser.pugx.org/phpwedge/text/downloads)](https://packagist.org/packages/phpwedge/text)
[![PHP Version Require](http://poser.pugx.org/phpwedge/text/require/php)](https://packagist.org/packages/phpwedge/text)
[![License](https://poser.pugx.org/phpwedge/text/license)](https://packagist.org/packages/phpwedge/text)

# phpwedge/text
> The library was made to easily convert text formatting between `normal text`, `PascalCase`, `camelCase`, `snake_case`, `kebab-case`, `SHOUTING_SNAKE_CASE` and `SHOUTING-KEBAB-CASE`.

## Installation
```bash
composer require phpwedge/text
```

## Usage
### Automatic conversion
> Determines the input text format automatically and converts it to the selected format type.

#### The `convert` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;
use PhpWedge\Core\Text\Type\CamelCaseText;

// Will output: thisIsSparta
echo TextConverter::getInstance()->convert(
    'this is sparta',
    CamelCaseText::class
);
```

#### The `convertToPascalCase` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;

// Will output: ThisIsSparta
echo TextConverter::getInstance()->convertToPascalCase('this-is-sparta');
```

#### The `convertToCamelCase` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;

// Will output: thisIsSparta
echo TextConverter::getInstance()->convertToCamelCase('this-is-sparta');
```

#### The `convertToSnakeCase` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;

// Will output: this_is_sparta
echo TextConverter::getInstance()->convertToSnakeCase('thisIsSparta');
```

#### The `convertToKebabCase` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;

// Will output: this-is-sparta
echo TextConverter::getInstance()->convertToKebabCase('thisIsSparta');
```

#### The `convertToShoutingSnakeCase` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;

// Will output: THIS_IS_SPARTA
echo TextConverter::getInstance()->convertToShoutingSnakeCase('thisIsSparta');
```

#### The `convertToShoutingKebabCase` method
```php
<?php
use PhpWedge\Core\Text\TextConverter;

// Will output: THIS-IS-SPARTA
echo TextConverter::getInstance()->convertToShoutingKebabCase('thisIsSparta');
```

### Manual conversion
> You can instantiate a format type and use them to convert to different format.

#### From not encoded text
```php
<?php
use PhpWedge\Core\Text\Type\CamelCaseText;
use PhpWedge\Core\Text\Type\KebabCaseText;

// Will return: thisIsSparta
$camelCaseText = new CamelCaseText('This is Sparta');
echo $camelCaseText->getText();

// Will return: this-is-sparta
$kebabCaseText = new KebabCaseText(
    $camelCaseText->getOriginalText()
);
echo $kebabCaseText->getText();
```

#### From encoded text
```php
<?php
use PhpWedge\Core\Text\Type\CamelCaseText;
use PhpWedge\Core\Text\Type\KebabCaseText;

// Will return: thisIsSparta
$camelCaseText = CamelCaseText::createFromEncodedText('thisIsSparta');
echo $camelCaseText->getText();

// Will return: this-is-sparta
$kebabCaseText = new KebabCaseText(
    $camelCaseText->getOriginalText()
);
echo $kebabCaseText->getText();
```
