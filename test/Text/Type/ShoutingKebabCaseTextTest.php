<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\ShoutingKebabCaseText;

class ShoutingKebabCaseTextTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $originalText
     *
     * @dataProvider provideTestCaseForTestGetText
     */
    public function testGetText(string $expected, string $originalText): void
    {
        self::assertEquals(
            $expected,
            (new ShoutingKebabCaseText($originalText))->getText()
        );
    }

    public function provideTestCaseForTestGetText(): array
    {
        return [
            [
                'THIS-IS-SPARTA',
                'This is Sparta',
            ],
            [
                'THIS-IS-SPARTA',
                ' This is Sparta',
            ],
            [
                'THIS-IS-SPARTA',
                'This is Sparta ',
            ],
            [
                'THIS-IS-SPARTA',
                ' This is Sparta ',
            ],
            [
                'THIS-IS-SPARTA',
                '-This is Sparta',
            ],
            [
                'THIS-IS-SPARTA',
                'This is Sparta-',
            ],
            [
                'THIS-IS-SPARTA',
                '-This is Sparta-',
            ],
            [
                'THIS-IS-SPARTA',
                '_This is Sparta',
            ],
            [
                'THIS-IS-SPARTA',
                'This is Sparta_',
            ],
            [
                'THIS-IS-SPARTA',
                '_This is Sparta_',
            ],
            [
                'THIS3-IS2-SPARTA1',
                '_This3 is2 Sparta1_',
            ],
        ];
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            ShoutingKebabCaseText::createFromEncodedText('THIS-IS-SPARTA')->getOriginalText()
        );
        self::assertEquals(
            'THIS-IS-SPARTA',
            ShoutingKebabCaseText::createFromEncodedText('THIS-IS-SPARTA')->getText()
        );
    }
}
