<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\SnakeCaseText;

class SnakeCaseTextTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $originalText
     *
     * @dataProvider provideTestCaseForTestGetText
     */
    public function testGetText(string $expected, string $originalText): void
    {
        self::assertEquals(
            $expected,
            (new SnakeCaseText($originalText))->getText()
        );
    }

    public function provideTestCaseForTestGetText(): array
    {
        return [
            [
                'this_is_sparta',
                'This is Sparta',
            ],
            [
                'this_is_sparta',
                ' This is Sparta',
            ],
            [
                'this_is_sparta',
                'This is Sparta ',
            ],
            [
                'this_is_sparta',
                ' This is Sparta ',
            ],
            [
                'this_is_sparta',
                '-This is Sparta',
            ],
            [
                'this_is_sparta',
                'This is Sparta-',
            ],
            [
                'this_is_sparta',
                '-This is Sparta-',
            ],
            [
                'this_is_sparta',
                '_This is Sparta',
            ],
            [
                'this_is_sparta',
                'This is Sparta_',
            ],
            [
                'this_is_sparta',
                '_This is Sparta_',
            ],
            [
                'this3_is2_sparta1',
                '_This3 is2 Sparta1_',
            ],
        ];
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            SnakeCaseText::createFromEncodedText('this_is_sparta')->getOriginalText()
        );
        self::assertEquals(
            'this_is_sparta',
            SnakeCaseText::createFromEncodedText('this_is_sparta')->getText()
        );
    }
}
