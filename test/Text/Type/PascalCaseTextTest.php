<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\PascalCaseText;

class PascalCaseTextTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $originalText
     *
     * @dataProvider provideTestCaseForTestGetText
     */
    public function testGetText(string $expected, string $originalText): void
    {
        self::assertEquals(
            $expected,
            (new PascalCaseText($originalText))->getText()
        );
    }

    public function provideTestCaseForTestGetText(): array
    {
        return [
            [
                'ThisIsSparta',
                'This is Sparta',
            ],
            [
                'ThisIsSparta',
                ' This is Sparta',
            ],
            [
                'ThisIsSparta',
                'This is Sparta ',
            ],
            [
                'ThisIsSparta',
                ' This is Sparta ',
            ],
            [
                'ThisIsSparta',
                '-This is Sparta',
            ],
            [
                'ThisIsSparta',
                'This is Sparta-',
            ],
            [
                'ThisIsSparta',
                '-This is Sparta-',
            ],
            [
                'ThisIsSparta',
                '_This is Sparta',
            ],
            [
                'ThisIsSparta',
                'This is Sparta_',
            ],
            [
                'ThisIsSparta',
                '_This is Sparta_',
            ],
            [
                'This3Is2Sparta1',
                '_This3 is2 Sparta1_',
            ],
        ];
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            PascalCaseText::createFromEncodedText('ThisIsSparta')->getOriginalText()
        );
        self::assertEquals(
            'ThisIsSparta',
            PascalCaseText::createFromEncodedText('ThisIsSparta')->getText()
        );
    }
}
