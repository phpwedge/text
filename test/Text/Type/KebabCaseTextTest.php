<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\KebabCaseText;

class KebabCaseTextTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $originalText
     *
     * @dataProvider provideTestCaseForTestGetText
     */
    public function testGetText(string $expected, string $originalText): void
    {
        self::assertEquals(
            $expected,
            (new KebabCaseText($originalText))->getText()
        );
    }

    public function provideTestCaseForTestGetText(): array
    {
        return [
            [
                'this-is-sparta',
                'This is Sparta',
            ],
            [
                'this-is-sparta',
                ' This is Sparta',
            ],
            [
                'this-is-sparta',
                'This is Sparta ',
            ],
            [
                'this-is-sparta',
                ' This is Sparta ',
            ],
            [
                'this-is-sparta',
                '-This is Sparta',
            ],
            [
                'this-is-sparta',
                'This is Sparta-',
            ],
            [
                'this-is-sparta',
                '-This is Sparta-',
            ],
            [
                'this-is-sparta',
                '_This is Sparta',
            ],
            [
                'this-is-sparta',
                'This is Sparta_',
            ],
            [
                'this-is-sparta',
                '_This is Sparta_',
            ],
            [
                'this3-is2-sparta1',
                '_This3 is2 Sparta1_',
            ],
        ];
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            KebabCaseText::createFromEncodedText('this-is-sparta')->getOriginalText()
        );
        self::assertEquals(
            'this-is-sparta',
            KebabCaseText::createFromEncodedText('this-is-sparta')->getText()
        );
    }
}
