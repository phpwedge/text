<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\ShoutingSnakeCaseText;

class ShoutingSnakeCaseTextTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $originalText
     *
     * @dataProvider provideTestCaseForTestGetText
     */
    public function testGetText(string $expected, string $originalText): void
    {
        self::assertEquals(
            $expected,
            (new ShoutingSnakeCaseText($originalText))->getText()
        );
    }

    public function provideTestCaseForTestGetText(): array
    {
        return [
            [
                'THIS_IS_SPARTA',
                'This is Sparta',
            ],
            [
                'THIS_IS_SPARTA',
                ' This is Sparta',
            ],
            [
                'THIS_IS_SPARTA',
                'This is Sparta ',
            ],
            [
                'THIS_IS_SPARTA',
                ' This is Sparta ',
            ],
            [
                'THIS_IS_SPARTA',
                '-This is Sparta',
            ],
            [
                'THIS_IS_SPARTA',
                'This is Sparta-',
            ],
            [
                'THIS_IS_SPARTA',
                '-This is Sparta-',
            ],
            [
                'THIS_IS_SPARTA',
                '_This is Sparta',
            ],
            [
                'THIS_IS_SPARTA',
                'This is Sparta_',
            ],
            [
                'THIS_IS_SPARTA',
                '_This is Sparta_',
            ],
            [
                'THIS3_IS2_SPARTA1',
                '_This3 is2 Sparta1_',
            ],
        ];
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            ShoutingSnakeCaseText::createFromEncodedText('THIS_IS_SPARTA')->getOriginalText()
        );
        self::assertEquals(
            'THIS_IS_SPARTA',
            ShoutingSnakeCaseText::createFromEncodedText('THIS_IS_SPARTA')->getText()
        );
    }
}
