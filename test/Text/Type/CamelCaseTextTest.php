<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\CamelCaseText;

class CamelCaseTextTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $originalText
     *
     * @dataProvider provideTestCaseForTestGetText
     */
    public function testGetText(string $expected, string $originalText): void
    {
        self::assertEquals(
            $expected,
            (new CamelCaseText($originalText))->getText()
        );
    }

    public function provideTestCaseForTestGetText(): array
    {
        return [
            [
                'thisIsSparta',
                'This is Sparta',
            ],
            [
                'thisIsSparta',
                ' This is Sparta',
            ],
            [
                'thisIsSparta',
                'This is Sparta ',
            ],
            [
                'thisIsSparta',
                ' This is Sparta ',
            ],
            [
                'thisIsSparta',
                '-This is Sparta',
            ],
            [
                'thisIsSparta',
                'This is Sparta-',
            ],
            [
                'thisIsSparta',
                '-This is Sparta-',
            ],
            [
                'thisIsSparta',
                '_This is Sparta',
            ],
            [
                'thisIsSparta',
                'This is Sparta_',
            ],
            [
                'thisIsSparta',
                '_This is Sparta_',
            ],
            [
                'this3Is2Sparta1',
                '_This3 is2 Sparta1_',
            ],
        ];
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            CamelCaseText::createFromEncodedText('thisIsSparta')->getOriginalText()
        );
        self::assertEquals(
            'thisIsSparta',
            CamelCaseText::createFromEncodedText('thisIsSparta')->getText()
        );
    }
}
