<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Type;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Type\SlugifiedText;

class SlugifiedTextTest extends TestCase
{
    public function testGetText(): void
    {
        self::assertEquals(
            'this-is-sparta',
            (new SlugifiedText('This is Spárta'))->getText()
        );
    }

    public function testCreateFromEncodedText(): void
    {
        self::assertEquals(
            'this is sparta',
            SlugifiedText::createFromEncodedText('this-is-sparta')->getOriginalText()
        );
        self::assertEquals(
            'this-is-sparta',
            SlugifiedText::createFromEncodedText('this-is-sparta')->getText()
        );
    }
}
