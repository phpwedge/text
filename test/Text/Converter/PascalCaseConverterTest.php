<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Converter;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Converter\PascalCaseConverter;
use PhpWedge\Core\Text\Type\PascalCaseText;

class PascalCaseConverterTest extends TestCase
{
    /**
     * @param bool $expected
     * @param string $text
     *
     * @dataProvider provideTestCaseForTestIsApplicable
     */
    public function testIsApplicable(bool $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            (new PascalCaseConverter())->isApplicable($text)
        );
    }

    public function provideTestCaseForTestIsApplicable(): array
    {
        return [
            [
                true,
                'AWorldApart',
            ],
            [
                true,
                'AWA',
            ],
            [
                true,
                'AW3A',
            ],
            [
                true,
                'AWA3',
            ],
            [
                false,
                '3WA',
            ],
            [
                true,
                'A3A',
            ],
            [
                true,
                'AW3',
            ],
            [
                true,
                'ThisIsSparta',
            ],
            [
                false,
                '3ThisIsSparta',
            ],
            [
                true,
                'This3IsSparta',
            ],
            [
                true,
                'ThisIs3Sparta',
            ],
            [
                true,
                'ThisIsSparta3',
            ],
            [
                true,
                'Id',
            ],
            [
                false,
                't',
            ],
            [
                false,
                'thisIsSparta',
            ],
            [
                false,
                'this_is_sparta',
            ],
            [
                false,
                'this-is-sparta',
            ],
            [
                false,
                'this is sparta',
            ],
        ];
    }

    public function testGetTextInstance(): void
    {
        $instance = (new PascalCaseConverter())->getTextInstance('ThisIsSparta');

        self::assertInstanceOf(PascalCaseText::class, $instance);
        self::assertEquals(
            'this is sparta',
            $instance->getOriginalText()
        );
    }
}
