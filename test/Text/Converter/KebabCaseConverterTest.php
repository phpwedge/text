<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Converter;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Converter\KebabCaseConverter;
use PhpWedge\Core\Text\Type\KebabCaseText;

class KebabCaseConverterTest extends TestCase
{
    /**
     * @param bool $expected
     * @param string $text
     *
     * @dataProvider provideTestCaseForTestIsApplicable
     */
    public function testIsApplicable(bool $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            (new KebabCaseConverter())->isApplicable($text)
        );
    }

    public function provideTestCaseForTestIsApplicable(): array
    {
        return [
            [
                true,
                'a-world-apart',
            ],
            [
                true,
                'a-w-a',
            ],
            [
                true,
                'a-w3-a',
            ],
            [
                true,
                'a-w-a3',
            ],
            [
                false,
                '3-w-a',
            ],
            [
                false,
                'a-3-a',
            ],
            [
                false,
                'a-w-3',
            ],
            [
                true,
                'this-is-sparta',
            ],
            [
                false,
                '3this-is-sparta',
            ],
            [
                true,
                'this3-is-sparta',
            ],
            [
                true,
                'this-is3-sparta',
            ],
            [
                true,
                'this-is-sparta3',
            ],
            [
                false,
                '-this-is-sparta',
            ],
            [
                false,
                'this-is-sparta-',
            ],
            [
                false,
                '-this-is-sparta-',
            ],
            [
                false,
                'this_is_sparta',
            ],
            [
                false,
                'this is sparta',
            ],
        ];
    }

    public function testGetTextInstance(): void
    {
        $instance = (new KebabCaseConverter())->getTextInstance('this-is-sparta');

        self::assertInstanceOf(KebabCaseText::class, $instance);
        self::assertEquals(
            'this is sparta',
            $instance->getOriginalText()
        );
    }
}
