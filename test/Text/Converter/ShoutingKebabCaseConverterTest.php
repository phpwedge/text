<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Converter;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Converter\ShoutingKebabCaseConverter;
use PhpWedge\Core\Text\Type\ShoutingKebabCaseText;

class ShoutingKebabCaseConverterTest extends TestCase
{
    /**
     * @param bool $expected
     * @param string $text
     *
     * @dataProvider provideTestCaseForTestIsApplicable
     */
    public function testIsApplicable(bool $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            (new ShoutingKebabCaseConverter())->isApplicable($text)
        );
    }

    public function provideTestCaseForTestIsApplicable(): array
    {
        return [
            [
                true,
                'A-WORLD-APART',
            ],
            [
                true,
                'A-W-A',
            ],
            [
                true,
                'A-W3-A',
            ],
            [
                true,
                'A-W-A3',
            ],
            [
                false,
                '3-W-A',
            ],
            [
                false,
                'A-3-A',
            ],
            [
                false,
                'A-W-3',
            ],
            [
                true,
                'THIS-IS-SPARTA',
            ],
            [
                false,
                '3THIS-IS-SPARTA',
            ],
            [
                true,
                'THIS3-IS-SPARTA',
            ],
            [
                true,
                'THIS-IS3-SPARTA',
            ],
            [
                true,
                'THIS-IS-SPARTA3',
            ],
            [
                false,
                '-THIS-IS-SPARTA',
            ],
            [
                false,
                'THIS-IS-SPARTA-',
            ],
            [
                false,
                '-THIS-IS-SPARTA-',
            ],
            [
                false,
                'THIS_IS_SPARTA',
            ],
            [
                false,
                'THIS IS SPARTA',
            ],
        ];
    }

    public function testGetTextInstance(): void
    {
        $instance = (new ShoutingKebabCaseConverter())->getTextInstance('THIS-IS-SPARTA');

        self::assertInstanceOf(ShoutingKebabCaseText::class, $instance);
        self::assertEquals(
            'this is sparta',
            $instance->getOriginalText()
        );
    }
}
