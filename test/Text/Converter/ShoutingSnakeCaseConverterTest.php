<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Converter;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Converter\ShoutingSnakeCaseConverter;
use PhpWedge\Core\Text\Type\ShoutingSnakeCaseText;

class ShoutingSnakeCaseConverterTest extends TestCase
{
    /**
     * @param bool $expected
     * @param string $text
     *
     * @dataProvider provideTestCaseForTestIsApplicable
     */
    public function testIsApplicable(bool $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            (new ShoutingSnakeCaseConverter())->isApplicable($text)
        );
    }

    public function provideTestCaseForTestIsApplicable(): array
    {
        return [
            [
                true,
                'A_WORLD_APART',
            ],
            [
                true,
                'A_W_A',
            ],
            [
                true,
                'A_W3_A',
            ],
            [
                true,
                'A_W_A3',
            ],
            [
                false,
                '3_W_A',
            ],
            [
                false,
                'A_3_A',
            ],
            [
                false,
                'A_W_3',
            ],
            [
                true,
                'THIS_IS_SPARTA',
            ],
            [
                false,
                '3THIS_IS_SPARTA',
            ],
            [
                true,
                'THIS3_IS_SPARTA',
            ],
            [
                true,
                'THIS_IS3_SPARTA',
            ],
            [
                true,
                'THIS_IS_SPARTA3',
            ],
            [
                false,
                '_THIS_IS_SPARTA',
            ],
            [
                false,
                'THIS_IS_SPARTA_',
            ],
            [
                false,
                '_THIS_IS_SPARTA_',
            ],
            [
                false,
                'THIS-IS-SPARTA',
            ],
            [
                false,
                'THIS IS SPARTA',
            ],
        ];
    }

    public function testGetTextInstance(): void
    {
        $instance = (new ShoutingSnakeCaseConverter())->getTextInstance('THIS_IS_SPARTA');

        self::assertInstanceOf(ShoutingSnakeCaseText::class, $instance);
        self::assertEquals(
            'this is sparta',
            $instance->getOriginalText()
        );
    }
}
