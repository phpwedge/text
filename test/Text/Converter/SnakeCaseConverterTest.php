<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Converter;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Converter\SnakeCaseConverter;
use PhpWedge\Core\Text\Type\SnakeCaseText;

class SnakeCaseConverterTest extends TestCase
{
    /**
     * @param bool $expected
     * @param string $text
     *
     * @dataProvider provideTestCaseForTestIsApplicable
     */
    public function testIsApplicable(bool $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            (new SnakeCaseConverter())->isApplicable($text)
        );
    }

    public function provideTestCaseForTestIsApplicable(): array
    {
        return [
            [
                true,
                'a_world_apart',
            ],
            [
                true,
                'a_w_a',
            ],
            [
                true,
                'a_w3_a',
            ],
            [
                true,
                'a_w_a3',
            ],
            [
                false,
                '3_w_a',
            ],
            [
                false,
                'a_3_a',
            ],
            [
                false,
                'a_w_3',
            ],
            [
                true,
                'this_is_sparta',
            ],
            [
                false,
                '3this_is_sparta',
            ],
            [
                true,
                'this3_is_sparta',
            ],
            [
                true,
                'this_is3_sparta',
            ],
            [
                true,
                'this_is_sparta3',
            ],
            [
                false,
                '_this_is_sparta',
            ],
            [
                false,
                'this_is_sparta_',
            ],
            [
                false,
                '_this_is_sparta_',
            ],
            [
                false,
                'this-is-sparta',
            ],
            [
                false,
                'this is sparta',
            ],
        ];
    }

    public function testGetTextInstance(): void
    {
        $instance = (new SnakeCaseConverter())->getTextInstance('this_is_sparta');

        self::assertInstanceOf(SnakeCaseText::class, $instance);
        self::assertEquals(
            'this is sparta',
            $instance->getOriginalText()
        );
    }
}
