<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text\Converter;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\Converter\CamelCaseConverter;
use PhpWedge\Core\Text\Type\CamelCaseText;

class CamelCaseConverterTest extends TestCase
{
    /**
     * @param bool $expected
     * @param string $text
     *
     * @dataProvider provideTestCaseForTestIsApplicable
     */
    public function testIsApplicable(bool $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            (new CamelCaseConverter())->isApplicable($text)
        );
    }

    public function provideTestCaseForTestIsApplicable(): array
    {
        return [
            [
                true,
                'aWorldApart',
            ],
            [
                true,
                'aWA',
            ],
            [
                true,
                'aW3A',
            ],
            [
                true,
                'aWA3',
            ],
            [
                false,
                '3WA',
            ],
            [
                true,
                'a3A',
            ],
            [
                true,
                'aW3',
            ],
            [
                true,
                'thisIsSparta',
            ],
            [
                false,
                '3thisIsSparta',
            ],
            [
                true,
                'this3IsSparta',
            ],
            [
                true,
                'thisIs3Sparta',
            ],
            [
                true,
                'thisIsSparta3',
            ],
            [
                true,
                'tId',
            ],
            [
                false,
                't',
            ],
            [
                false,
                'id',
            ],
            [
                false,
                'ThisIsSparta',
            ],
            [
                false,
                'this_is_sparta',
            ],
            [
                false,
                'this-is-sparta',
            ],
            [
                false,
                'this is sparta',
            ],
        ];
    }

    public function testGetTextInstance(): void
    {
        $instance = (new CamelCaseConverter())->getTextInstance('thisIsSparta');

        self::assertInstanceOf(CamelCaseText::class, $instance);
        self::assertEquals(
            'this is sparta',
            $instance->getOriginalText()
        );
    }
}
