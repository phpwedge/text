<?php

declare(strict_types=1);


namespace PhpWedgeTest\Core\Text;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Text\TextConverter;
use PhpWedge\Core\Text\Type\CamelCaseText;
use PhpWedge\Core\Text\Type\KebabCaseText;
use PhpWedge\Core\Text\Type\PascalCaseText;
use PhpWedge\Core\Text\Type\ShoutingKebabCaseText;
use PhpWedge\Core\Text\Type\ShoutingSnakeCaseText;
use PhpWedge\Core\Text\Type\SnakeCaseText;

class TextConverterTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $inputText
     * @param string $outputFormatFQN
     *
     * @dataProvider provideTestCasesFormTestConvert
     */
    public function testConvert(string $expected, string $inputText, string $outputFormatFQN): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convert($inputText, $outputFormatFQN)
        );
    }

    public function provideTestCasesFormTestConvert(): array
    {
        return [
            /* PascalCase */
            'pascalCaseToPascalCase' => [
                'ThisIsSparta',
                'ThisIsSparta',
                PascalCaseText::class,
            ],
            'pascalCaseToCamelCase' => [
                'thisIsSparta',
                'ThisIsSparta',
                CamelCaseText::class,
            ],
            'pascalCaseToSnakeCase' => [
                'this_is_sparta',
                'ThisIsSparta',
                SnakeCaseText::class,
            ],
            'pascalCaseToKebabCase' => [
                'this-is-sparta',
                'ThisIsSparta',
                KebabCaseText::class,
            ],
            'pascalCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'ThisIsSparta',
                ShoutingSnakeCaseText::class,
            ],
            'pascalCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'ThisIsSparta',
                ShoutingKebabCaseText::class,
            ],

            /* camelCase */
            'camelCaseToCamelCase' => [
                'thisIsSparta',
                'thisIsSparta',
                CamelCaseText::class,
            ],
            'camelCaseToPascalCase' => [
                'ThisIsSparta',
                'thisIsSparta',
                PascalCaseText::class,
            ],
            'camelCaseToSnakeCase' => [
                'this_is_sparta',
                'thisIsSparta',
                SnakeCaseText::class,
            ],
            'camelCaseToKebabCase' => [
                'this-is-sparta',
                'thisIsSparta',
                KebabCaseText::class,
            ],
            'camelCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'thisIsSparta',
                ShoutingSnakeCaseText::class,
            ],
            'camelCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'thisIsSparta',
                ShoutingKebabCaseText::class,
            ],

            /* snake_case */
            'snakeCaseToSnakeCase' => [
                'this_is_sparta',
                'this_is_sparta',
                SnakeCaseText::class,
            ],
            'snakeCaseToPascalCase' => [
                'ThisIsSparta',
                'this_is_sparta',
                PascalCaseText::class,
            ],
            'snakeCaseToCamelCase' => [
                'thisIsSparta',
                'this_is_sparta',
                CamelCaseText::class,
            ],
            'snakeCaseToKebabCase' => [
                'this-is-sparta',
                'this_is_sparta',
                KebabCaseText::class,
            ],
            'snakeCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'this_is_sparta',
                ShoutingSnakeCaseText::class,
            ],
            'snakeCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'this_is_sparta',
                ShoutingKebabCaseText::class,
            ],

            /* kebab-case */
            'kebabCaseToKebabCase' => [
                'this-is-sparta',
                'this-is-sparta',
                KebabCaseText::class,
            ],
            'kebabCaseToPascalCase' => [
                'ThisIsSparta',
                'this-is-sparta',
                PascalCaseText::class,
            ],
            'kebabCaseToCamelCase' => [
                'thisIsSparta',
                'this-is-sparta',
                CamelCaseText::class,
            ],
            'kebabCaseToSnakeCase' => [
                'this_is_sparta',
                'this-is-sparta',
                SnakeCaseText::class,
            ],
            'kebabCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'this-is-sparta',
                ShoutingSnakeCaseText::class,
            ],
            'kebabCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'this-is-sparta',
                ShoutingKebabCaseText::class,
            ],

            /* SHOUTING_SNAKE_CASE */
            'shoutingSnakeCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'THIS_IS_SPARTA',
                ShoutingSnakeCaseText::class,
            ],
            'shoutingSnakeCaseToPascalCase' => [
                'ThisIsSparta',
                'THIS_IS_SPARTA',
                PascalCaseText::class,
            ],
            'shoutingSnakeCaseToCamelCase' => [
                'thisIsSparta',
                'THIS_IS_SPARTA',
                CamelCaseText::class,
            ],
            'shoutingSnakeCaseToSnakeCase' => [
                'this_is_sparta',
                'THIS_IS_SPARTA',
                SnakeCaseText::class,
            ],
            'shoutingSnakeCaseToKebabCase' => [
                'this-is-sparta',
                'THIS_IS_SPARTA',
                KebabCaseText::class,
            ],
            'shoutingSnakeCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'THIS_IS_SPARTA',
                ShoutingKebabCaseText::class,
            ],

            /* SHOUTING-KEBAB-CASE */
            'shoutingKebabCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'THIS-IS-SPARTA',
                ShoutingKebabCaseText::class,
            ],
            'shoutingKebabCaseToPascalCase' => [
                'ThisIsSparta',
                'THIS-IS-SPARTA',
                PascalCaseText::class,
            ],
            'shoutingKebabCaseToCamelCase' => [
                'thisIsSparta',
                'THIS-IS-SPARTA',
                CamelCaseText::class,
            ],
            'shoutingKebabCaseToSnakeCase' => [
                'this_is_sparta',
                'THIS-IS-SPARTA',
                SnakeCaseText::class,
            ],
            'shoutingKebabCaseToKebabCase' => [
                'this-is-sparta',
                'THIS-IS-SPARTA',
                KebabCaseText::class,
            ],
            'shoutingKebabCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'THIS-IS-SPARTA',
                ShoutingSnakeCaseText::class,
            ],

            /** Plain text */
            'plainTextToPascalCase' => [
                'ThisIsSparta',
                'This-is_Sparta_',
                PascalCaseText::class,
            ],
            'plainTextToCamelCase' => [
                'thisIsSparta',
                'This-is_Sparta_',
                CamelCaseText::class,
            ],
            'plainTextToSnakeCase' => [
                'this_is_sparta',
                'This-is_Sparta_',
                SnakeCaseText::class,
            ],
            'plainTextToKebabCase' => [
                'this-is-sparta',
                'This-is_Sparta_',
                KebabCaseText::class,
            ],
            'plainTextToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'This-is_Sparta_',
                ShoutingSnakeCaseText::class,
            ],
            'plainTextToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'This-is_Sparta_',
                ShoutingKebabCaseText::class,
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $inputText
     *
     * @dataProvider provideTestCasesFormTestConvertToPascalCase
     */
    public function testConvertToPascalCase(string $expected, string $inputText): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convertToPascalCase($inputText)
        );
    }

    public function provideTestCasesFormTestConvertToPascalCase(): array
    {
        return [
            'pascalCaseToPascalCase' => [
                'ThisIsSparta',
                'ThisIsSparta',
            ],
            'pascalCaseToCamelCase' => [
                'ThisIsSparta',
                'thisIsSparta',
            ],
            'pascalCaseToSnakeCase' => [
                'ThisIsSparta',
                'this_is_sparta',
            ],
            'pascalCaseToKebabCase' => [
                'ThisIsSparta',
                'this-is-sparta',
            ],
            'pascalCaseToShoutingSnakeCase' => [
                'ThisIsSparta',
                'THIS_IS_SPARTA',
            ],
            'pascalCaseToShoutingKebabCase' => [
                'ThisIsSparta',
                'THIS-IS-SPARTA',
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $inputText
     *
     * @dataProvider provideTestCasesFormTestConvertToCamelCase
     */
    public function testConvertToCamelCase(string $expected, string $inputText): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convertToCamelCase($inputText)
        );
    }

    public function provideTestCasesFormTestConvertToCamelCase(): array
    {
        return [
            'camelCaseToCamelCase' => [
                'thisIsSparta',
                'thisIsSparta',
            ],
            'camelCaseToPascalCase' => [
                'thisIsSparta',
                'ThisIsSparta',
            ],
            'camelCaseToSnakeCase' => [
                'thisIsSparta',
                'this_is_sparta',
            ],
            'camelCaseToKebabCase' => [
                'thisIsSparta',
                'this-is-sparta',
            ],
            'camelCaseToShoutingSnakeCase' => [
                'thisIsSparta',
                'THIS_IS_SPARTA',
            ],
            'camelCaseToShoutingKebabCase' => [
                'thisIsSparta',
                'THIS-IS-SPARTA',
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $inputText
     *
     * @dataProvider provideTestCasesFormTestConvertToSnakeCase
     */
    public function testConvertToSnakeCase(string $expected, string $inputText): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convertToSnakeCase($inputText)
        );
    }

    public function provideTestCasesFormTestConvertToSnakeCase(): array
    {
        return [
            'snakeCaseToSnakeCase' => [
                'this_is_sparta',
                'this_is_sparta',
            ],
            'snakeCaseToPascalCase' => [
                'this_is_sparta',
                'ThisIsSparta',
            ],
            'snakeCaseToCamelCase' => [
                'this_is_sparta',
                'thisIsSparta',
            ],
            'snakeCaseToKebabCase' => [
                'this_is_sparta',
                'this-is-sparta',
            ],
            'snakeCaseToShoutingSnakeCase' => [
                'this_is_sparta',
                'THIS_IS_SPARTA',
            ],
            'snakeCaseToShoutingKebabCase' => [
                'this_is_sparta',
                'THIS-IS-SPARTA',
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $inputText
     *
     * @dataProvider provideTestCasesFormTestConvertToKebabCase
     */
    public function testConvertToKebabCase(string $expected, string $inputText): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convertToKebabCase($inputText)
        );
    }

    public function provideTestCasesFormTestConvertToKebabCase(): array
    {
        return [
            'kebabCaseToKebabCase' => [
                'this-is-sparta',
                'this-is-sparta',
            ],
            'kebabCaseToCamelCase' => [
                'this-is-sparta',
                'thisIsSparta',
            ],
            'kebabCaseToPascalCase' => [
                'this-is-sparta',
                'ThisIsSparta',
            ],
            'kebabCaseToSnakeCase' => [
                'this-is-sparta',
                'this_is_sparta',
            ],
            'kebabCaseToShoutingSnakeCase' => [
                'this-is-sparta',
                'THIS_IS_SPARTA',
            ],
            'kebabCaseToShoutingKebabCase' => [
                'this-is-sparta',
                'THIS-IS-SPARTA',
            ],
        ];
    }


    /**
     * @param string $expected
     * @param string $inputText
     *
     * @dataProvider provideTestCasesFormTestConvertToShoutingSnakeCase
     */
    public function testConvertToShoutingSnakeCase(string $expected, string $inputText): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convertToShoutingSnakeCase($inputText)
        );
    }

    public function provideTestCasesFormTestConvertToShoutingSnakeCase(): array
    {
        return [
            'shoutingSnakeCaseToShoutingSnakeCase' => [
                'THIS_IS_SPARTA',
                'THIS_IS_SPARTA',
            ],
            'shoutingSnakeCaseToPascalCase' => [
                'THIS_IS_SPARTA',
                'ThisIsSparta',
            ],
            'shoutingSnakeCaseToCamelCase' => [
                'THIS_IS_SPARTA',
                'thisIsSparta',
            ],
            'shoutingSnakeCaseToSnakeCase' => [
                'THIS_IS_SPARTA',
                'this_is_sparta',
            ],
            'shoutingSnakeCaseToKebabCase' => [
                'THIS_IS_SPARTA',
                'this-is-sparta',
            ],
            'shoutingSnakeCaseToShoutingKebabCase' => [
                'THIS_IS_SPARTA',
                'THIS-IS-SPARTA',
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $inputText
     *
     * @dataProvider provideTestCasesFormTestConvertToShoutingKebabCase
     */
    public function testConvertToShoutingKebabCase(string $expected, string $inputText): void
    {
        self::assertEquals(
            $expected,
            TextConverter::getInstance()->convertToShoutingKebabCase($inputText)
        );
    }

    public function provideTestCasesFormTestConvertToShoutingKebabCase(): array
    {
        return [
            'shoutingKebabCaseToShoutingKebabCase' => [
                'THIS-IS-SPARTA',
                'THIS-IS-SPARTA',
            ],
            'shoutingKebabCaseToCamelCase' => [
                'THIS-IS-SPARTA',
                'thisIsSparta',
            ],
            'shoutingKebabCaseToPascalCase' => [
                'THIS-IS-SPARTA',
                'ThisIsSparta',
            ],
            'shoutingKebabCaseToSnakeCase' => [
                'THIS-IS-SPARTA',
                'this_is_sparta',
            ],
            'shoutingKebabCaseToKebabCase' => [
                'THIS-IS-SPARTA',
                'this-is-sparta',
            ],
            'shoutingKebabCaseToShoutingSnakeCase' => [
                'THIS-IS-SPARTA',
                'THIS_IS_SPARTA',
            ],
        ];
    }
}
